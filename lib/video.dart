class Video {
  late String id;
  late String title;
  late String videoId;
  late String url;
  late String publishedAt;
  late String thumbnail;

  Video(
    this.id,
    this.videoId,
    this.url,
    this.title,
    this.publishedAt,
    this.thumbnail
  );

  Video.fromJson(Map<String, dynamic> parsedJson){
    this.id = parsedJson['id'];
    this.videoId = parsedJson['snippet']['resourceId']['videoId'];
    this.url = 'https://www.youtube.com/watch?v=' + this.videoId;
    this.title = parsedJson['snippet']['title'];
    this.publishedAt = parsedJson['snippet']['publishedAt'];
    this.thumbnail = parsedJson['snippet']['thumbnails']['high']['url'];
  }
}
